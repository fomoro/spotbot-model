from __future__ import print_function
from __future__ import division

import os
import time
import numpy as np
import tensorflow as tf

from model import Model
from utilities import get_data_stream

BATCH_SIZE = 35 # TODO: try reducing this?
EPOCHS = 20
CHECKPOINT_INTERVAL = 10

flags = tf.app.flags
FLAGS = flags.FLAGS

# define flags
flags.DEFINE_boolean('restore', True, 'If true, restore from checkpoints.')

# define artifact directories where results from the session can be saved
dataset_path = os.environ.get('DATASET_PATH', 'datasets/spotbot.hdf5')
checkpoint_path = os.environ.get('CHECKPOINT_PATH', 'checkpoints/')

data_train, stream_train = get_data_stream(dataset_path, which_sets=['train'], batch_size=BATCH_SIZE)
data_valid, stream_valid = get_data_stream(dataset_path, which_sets=['valid'], batch_size=BATCH_SIZE)
data_test, stream_test = get_data_stream(dataset_path, which_sets=['test'], batch_size=BATCH_SIZE)

X_valid, y_valid = stream_valid.get_epoch_iterator().next()
X_test, y_test = stream_test.get_epoch_iterator().next()

with tf.Graph().as_default(), tf.Session() as sess:
    model = Model(restore=FLAGS.restore)

    for epoch in range(EPOCHS):
        for batch_x, batch_y in stream_train.get_epoch_iterator():
            batch_size = batch_x.shape[0]
            if batch_size != BATCH_SIZE:
                continue

            _, train_loss, global_step, summary = sess.run([model.train_op, model.loss_op, model.global_step, model.summaries_op], feed_dict={
                model.x: batch_x,
                model.y_: batch_y,
                model.dropout: 0.5
            })
            train_loss /= batch_size

            model.summary_writer.add_summary(summary, global_step=global_step)
            print('[train] epoch: %d, global step: %d, loss: %g' % (epoch, global_step, train_loss))

            if global_step % CHECKPOINT_INTERVAL == 0:
                model.saver.save(sess, checkpoint_path + 'checkpoint', global_step=global_step)

        valid_loss, valid_accuracy = 0, 0
        num_batches = 0
        for batch_x, batch_y in stream_valid.get_epoch_iterator():
            loss, accuracy = sess.run([model.loss_op, model.accuracy_op], feed_dict={
                model.x: X_valid,
                model.y_: y_valid,
                model.dropout: 1.0
            })
            valid_loss += loss
            valid_accuracy += accuracy
            num_batches += 1

        valid_loss /= data_valid.num_examples
        valid_accuracy /= num_batches
        print('[valid] epoch: %d, loss: %g, accuracy: %g' % (epoch, valid_loss, valid_accuracy))

    test_loss, test_accuracy = 0, 0
    num_batches = 0
    for batch_x, batch_y in stream_test.get_epoch_iterator():
        loss, accuracy = sess.run([model.loss_op, model.accuracy_op], feed_dict={
            model.x: X_test,
            model.y_: y_test,
            model.dropout: 1.0
        })
        test_loss += loss
        test_accuracy += accuracy
        num_batches += 1

    test_loss /= data_test.num_examples
    test_accuracy /= num_batches
    print('[test] loss: %g, accuracy: %g' % (test_loss, test_accuracy))

    model.summary_writer.close()
