import os
import time
import numpy as np
import tensorflow as tf

from utilities import dense_layer, conv2d_layer, highway_conv2d_layer, flatten, dropout

NUM_CLASSES = 4
WIDTH, HEIGHT = 256, 384 # TODO: try reducing this?

checkpoint_path = os.environ.get('CHECKPOINT_PATH', 'checkpoints/')
summary_path = os.environ.get('SUMMARY_PATH', 'logs/')
model_path = os.environ.get('MODEL_PATH', 'models/')

class Model:
    def __init__(self, restore=False):
        self.sess = tf.get_default_session()

        self.global_step = tf.Variable(0, trainable=False, name='global_step')
        global_step_op = self.global_step.assign_add(1)

        self.x = tf.placeholder("float", [None, HEIGHT, WIDTH, 3])
        self.y_ = tf.placeholder("float", [None, NUM_CLASSES])

        prev_y = conv2d_layer(self.x, [3, 3, 3, 32], [1, 1, 1, 1], 'VALID')
        prev_y = conv2d_layer(prev_y, [3, 3, 32, 32], [1, 1, 1, 1], 'VALID')
        prev_y = tf.nn.max_pool(prev_y, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')

        prev_y = conv2d_layer(prev_y, [3, 3, 32, 64], [1, 1, 1, 1], 'VALID')
        prev_y = conv2d_layer(prev_y, [3, 3, 64, 64], [1, 1, 1, 1], 'VALID')
        prev_y = tf.nn.max_pool(prev_y, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')

        prev_y = conv2d_layer(prev_y, [3, 3, 64, 128], [1, 1, 1, 1], 'VALID')
        prev_y = conv2d_layer(prev_y, [3, 3, 128, 128], [1, 1, 1, 1], 'VALID')
        prev_y = tf.nn.max_pool(prev_y, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')

        prev_y, layer_size = flatten(prev_y)
        prev_y = dense_layer(prev_y, [layer_size, 128], tf.sigmoid)
        prev_y, self.dropout = dropout(prev_y, name="dropout")
        self.y = dense_layer(prev_y, [128, NUM_CLASSES], tf.nn.softmax)

        # define training and accuracy operations
        with tf.name_scope("loss"):
            self.loss_op = -tf.reduce_sum(self.y_ * tf.log(self.y + 1e-12))
            tf.scalar_summary("loss", self.loss_op)

            loss_ema = tf.train.ExponentialMovingAverage(decay=0.9, num_updates=self.global_step)
            loss_ema_op = loss_ema.apply([self.loss_op])
            tf.scalar_summary('loss_ema', loss_ema.average(self.loss_op))

        with tf.name_scope("test"):
            cross_entropy = tf.equal(tf.argmax(self.y, 1), tf.argmax(self.y_, 1))

            self.accuracy_op = tf.reduce_mean(tf.cast(cross_entropy, "float"))
            tf.scalar_summary('accuracy', self.accuracy_op)

            accuracy_ema = tf.train.ExponentialMovingAverage(decay=0.9, num_updates=self.global_step)
            accuracy_ema_op = accuracy_ema.apply([self.accuracy_op])
            tf.scalar_summary('accuracy_ema', accuracy_ema.average(self.accuracy_op))

        with tf.control_dependencies([global_step_op, accuracy_ema_op, loss_ema_op]):
            self.train_op = tf.train.AdamOptimizer(1e-3).minimize(self.loss_op, name='train')

        self.summaries_op = tf.merge_all_summaries()

        # create a saver instance to restore from the checkpoint
        self.saver = tf.train.Saver(max_to_keep=1)

        # initialize our variables
        self.sess.run(tf.initialize_all_variables())

        # save the graph definition as a protobuf file
        tf.train.write_graph(self.sess.graph_def, model_path, 'spotbot.pb', as_text=False)

        # restore variables
        if restore:
            latest_checkpoint_path = tf.train.latest_checkpoint(checkpoint_path)
            if latest_checkpoint_path:
                print('Restoring checkpoint: {0}'.format(latest_checkpoint_path))
                self.saver.restore(self.sess, latest_checkpoint_path)

        self.summary_writer = tf.train.SummaryWriter('{0}{1}'.format(summary_path, int(time.time()), self.sess.graph_def))
