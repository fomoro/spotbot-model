from __future__ import print_function
from __future__ import division

import os
import numpy as np
import tensorflow as tf

from fuel.datasets import H5PYDataset
from fuel.schemes import ShuffledScheme
from fuel.streams import DataStream

def get_data_stream(dataset_path, which_sets, batch_size=None):
    dataset = H5PYDataset(dataset_path, which_sets=which_sets, load_in_memory=False)
    if batch_size is None:
        scheme = ShuffledScheme(examples=dataset.num_examples, batch_size=dataset.num_examples)
    else:
        scheme = ShuffledScheme(examples=dataset.num_examples, batch_size=batch_size)
    stream = DataStream(dataset=dataset, iteration_scheme=scheme)
    return dataset, stream

def dropout(x, name=None):
    keep_prob = tf.placeholder("float", name=name)
    return tf.nn.dropout(x, keep_prob), keep_prob

def weight_bias(shape, bias_init=0.1):
    W = tf.Variable(tf.truncated_normal(shape, stddev=0.1), name='weight')
    b = tf.Variable(tf.constant(bias_init, shape=shape[-1:]), name='bias')
    return W, b

def dense_layer(x, shape, activation):
    W, b = weight_bias(shape)
    return activation(tf.matmul(x, W) + b)

def conv2d_layer(x, shape, strides, padding):
    W, b = weight_bias(shape)
    return tf.nn.relu(tf.nn.conv2d(x, W, strides, padding) + b)

def flatten(x):
    shape = x.get_shape()
    dim = shape[1] * shape[2] * shape[3]
    return tf.reshape(x, [-1, dim.value]), dim.value

def highway_conv2d_layer(x, shape, strides, padding, carry_bias=-1.0):
    W, b = weight_bias(shape, carry_bias)
    W_T, b_T = weight_bias(shape)
    H = tf.nn.relu(tf.nn.conv2d(x, W, strides, padding) + b, name='activation')
    T = tf.sigmoid(tf.nn.conv2d(x, W_T, strides, padding) + b_T, name='transform_gate')
    C = tf.sub(1.0, T, name="carry_gate")
    return tf.add(tf.mul(H, T), tf.mul(x, C), 'y') # y = (H * T) + (x * C)
